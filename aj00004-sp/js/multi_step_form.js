$(function(){
	/* 変数定義 */
	var layer       = $('.layer'),
		layerLength = layer.length,
		layerFlag   = [false, false, false, false, false],
		current     = 0,

		step        = $('.step'),
		btn         = $('.trnsBtn'),
		blurElm     = $('.blurElm'),
		cover       = $('.cover'),

		slideTrg    = $('.slideTrg'),
		slctBtn     = null,
		slctItem    = $('input[type=radio], input[type=checkbox]'),
		clsBtn      = $('.clsBtn'),
		inputMail   = $('#fMail'),
		inputTel    = $('#fTel'),
		form        = $('form');

	var birthFlg = false; // 生年月日用のフラグ

	/* ステップの初期化 */
	initStep();

	/* レイヤーの初期化 */
	for (var i = 0; i < layerLength; i++) {
		layer.eq(i).css('z-index', Math.pow(10, i));
	}

	/* 次へボタンのアクティブ化判定 */
	discBtn();

	var setGroup = 'first';
	/* リアルタイムバリデーション */
	$('select, input').not('.noRealTimeValid').on('change blur', function(event) {
		if ($(this).hasClass('validGroup')) {
			// 選択中のグループを格納
			setGroup = $(this).attr('data-group');

			// グループ全てが選択/入力されていたらエラーチェックする
			grpElm = $('[data-group="'+setGroup+'"]');
			grpVal = [];
			for (var i = 0; i < grpElm.length; i++) {
				grpVal.push(grpElm.eq(i).val());
			}
			criterion = (grpVal.indexOf('') != -1);

			if(criterion == false){
				setValidation(grpElm, criterion, setGroup);
			}
		} else {
			criterion = (!$(this).val() || $(this).val() == 'null');
			
			setValidation($(this), criterion, $(this).attr('id'));
		}

	});

	$('select, input').not('.noRealTimeValid').on('focus', function(event) {
		// フォーカスしたグループが選択中のグループと同じ、または一番始めのグループだった場合、処理を行わない
		if(setGroup != $(this).attr('data-group') && setGroup != 'first'){
			// 選択中のグループと違う場合、エラーチェックを行う
			grpElm = $('[data-group="'+setGroup+'"]');
			grpVal = [];
			for (var i = 0; i < grpElm.length; i++) {
				grpVal.push(grpElm.eq(i).val());
			}
			criterion = (grpVal.indexOf('') != -1);

			setValidation(grpElm, criterion, setGroup);
		}
	});

	// フォーム内のどこかをクリックした場合に選択中のグループのエラーチェックを行う
	form.on('click', function(e){
		if(e.target.tagName != 'SELECT' && e.target.tagName != 'INPUT' && e.target.tagName != 'OPTION'){
			grpElm = $('[data-group="'+setGroup+'"]');
			grpVal = [];
			for (var i = 0; i < grpElm.length; i++) {
				grpVal.push(grpElm.eq(i).val());
			}
			criterion = (grpVal.indexOf('') != -1);

			setValidation(grpElm, criterion, setGroup);
		}
	});

	function setValidation(target, criterion, type){
		is_valid = (criterion) ? true : false ;
		switch(type){
			case 'birthday' :
				if (is_valid == true) {
					target.parents('dd').addClass('error').prev().find('.errorTxt').show().html('【注】未選択です');
				} else {
					// 生年月日判定
					birth_y = target.eq(0).val(),
					birth_m = target.eq(1).val(),
					birth_d = target.eq(2).val(),
					birth = new Date(birth_y, birth_m-1, birth_d);

					// 生年月日の存在チェック
					if(!(birth.getFullYear()==birth_y && birth.getMonth()==birth_m-1 && birth.getDate()==birth_d)){
						target.parents('dd').addClass('error').prev().find('.errorTxt').show().html('【注】存在しない日付です');
					}else{
						target.parents('dd').removeClass('error').prev().find('.errorTxt').hide();
					}
				}
				layerValid();
				break;

			case 'name' :
				if (is_valid == true) {
					target.parents('dd').addClass('error').prev().find('.errorTxt').show();
				} else {
					target.parents('dd').removeClass('error').prev().find('.errorTxt').hide();
				}
				break;

			case 'ruby' :
				if (is_valid == true) {
					target.parents('dd').addClass('error').prev().find('.errorTxt').show().html('【注】未入力です');
				} else {
					if(!target.eq(0).val().match(/^[\u30a0-\u30ff\u3040-\u309f]+$/) || !target.eq(1).val().match(/^[\u30a0-\u30ff\u3040-\u309f]+$/)){
						target.parents('dd').addClass('error').prev().find('.errorTxt').show().html('【注】ひらがなまたはカタカナでご入力ください。');
					}else{
						target.parents('dd').removeClass('error').prev().find('.errorTxt').hide();
					}
				}
				layerValid();
				break;

			case 'fMail' :
				if (is_valid == true) {
					target.parents('dd').addClass('error').prev().find('.errorTxt').show().html('【注】未入力です');
				} else {
					if(!target.val().match(/.+@.+\..+/)){
						target.parents('dd').addClass('error').prev().find('.errorTxt').show().html('【注】メールアドレスが正しくありません。');
					}else{
						target.parents('dd').removeClass('error').prev().find('.errorTxt').hide();
					}
				}
				break;

			case 'fTel' :
				if (is_valid == true) {
					target.parents('dd').addClass('error').prev().find('.errorTxt').show().html('【注】未入力です');
				} else {
					// tel判定
					// ハイフンが入っていたら強制的に削除
					var val = target.val().replace(/[━.*‐.*―.*－.*\?.*ー.*\-]/gi,'');
					// 全角 ⇒ 半角
					val = val.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
					target.val(val);

					var errTxt = '';
					if(val.match(/^\d{11}$/)){
						if(val.match(/^0[5|7|8|9]0/)){
							errTxt = '';
						}else{
							errTxt = '【注】電話番号の形式が不正です';
						}
					}else if(val.match(/^\d{10}$/)){
						if (val.match(/^0/)) {
							if (val.match(/^0[5|7|8|9]0/)) {
								errTxt = '【注】桁数が少ないです';
							} else {
								errTxt = '';
							}
						} else {
							errTxt = '【注】電話番号の形式が不正です';
						}
					}else{
						if(val.match(/\d{12,}$/)){
							errTxt = '【注】桁数が多いです';
						}else if(val.match(/\d{1,9}$/)){
							errTxt = '【注】桁数が少ないです';
						}else{
							errTxt = '【注】電話番号は半角数字で入力してください';
						}
					}

					if(errTxt){
						target.parents('dd').addClass('error').prev().find('.errorTxt').show().html(errTxt);
					}else{
						// 上記エラーに該当しなければ
						target.parents('dd').removeClass('error').prev().find('.errorTxt').hide();
					}
				}
				break;

			default :
				if (is_valid == true) {
					target.parents('dd').addClass('error').prev().find('.errorTxt').show();
				} else {
					target.parents('dd').removeClass('error').prev().find('.errorTxt').hide();
				}
				break;
		}

	}

	/* レイヤーの出し入れ */
	btn.on('click', function(event) {
		if (blurElm.is(':focus')) {
			blurElm.blur();
		} else {
			($(this).hasClass('subBtn')) ? trnsNext() : trnsPrev() ;
		}
	});

	/* スライドパネルのトリガーをクリックしたら */
	slideTrg.on('click', function(event) {
		panelName = $(this).attr('data-panelname');
		panel = $('#'+panelName);
		crntVal = [];
		panelInput = panel.find('input');
		for (var i = 0; i < panelInput.length; i++) {
			if (panelInput.eq(i).prop('checked') === true) {
				crntVal.push(panelInput.eq(i).val());
			}
		}
		slctBtn = panel.find('.selectBtn');
		panelIn();		// スライドパネルを表示する
		value = [];
		checkItem();	// 選択肢のチェックの有無を判定
		discSelect();	// 決定ボタンのアクティブ化判定
	});

	/* クローズボタンをクリックしたら */
	clsBtn.click(function(event) {
		panelOut();	// スライドパネルを閉じる
		for (var i = 0; i < panelInput.length; i++) {
			crntProp = (crntVal.indexOf(panelInput.eq(i).val()) != -1) ? true : false ;
			panelInput.eq(i).prop('checked', crntProp);
		}
		_this = $('[data-panelname="'+$(this).parents('.slidePanel').attr('id')+'"]');
		if (crntVal.length == 0) {
			_this.parents('dd').addClass('error').prev().find('.errorTxt').show();
		} else {
			_this.parents('dd').removeClass('error').prev().find('.errorTxt').hide();
		}

		emptyFlag = true;
		discSelect();	// 決定ボタンのアクティブ化判定
	});



	/* 選択肢をクリックしたら */
	slctItem.click(function(event) {
		emptyFlag = true;
		value = [];
		licenseTxt = $('.license_other textarea');
		checkItem();	// 選択肢のチェックの有無を判定
		discSelect();	// 決定ボタンのアクティブ化判定

		/* 選択された項目がその他だった場合 */
		if ($(this).hasClass('other')) {
			if ($(this).prop('checked')) {
				licenseTxt.prop('disabled', false);
			} else {
				licenseTxt.prop('disabled', true);
			}
		}

		/* 決定ボタンをクリックしたら */
		slctBtn.find('button').click(function(event) {
			inputValue();	// 選択肢の値を表示
			discStepNum();	// スキップする場合に該当のレイヤーを非表示
			panelOut();		// スライドパネルを閉じる
			$('[data-panelname="'+$(this).parents('.slidePanel').attr('id')+'"]').parents('dd').removeClass('error').prev().find('.errorTxt').hide();
		});
	});

	/* 電話番号のハイフン外し */
	inputTel.blur(function(event) {
		txtTel = inputTel.val();
		txtTel = txtTel.replace(new RegExp('-', "g"), '');
		inputTel.val(txtTel)
	});

	form.submit(function(event) {
		validFlag = submitValid();
		if (validFlag == false) return false;
	});

	function submitValid() {
		mailFlag = mailchk();
		telFlag  = tellchk();
		if (mailFlag == false || telFlag != '') {
			var errorText = '以下の項目を正しく入力してください。\n';
			if (mailFlag == false) errorText += '・メールアドレス\n　形式が不正です。';
			if (mailFlag == false && telFlag) errorText += '\n';
			if (telFlag != '') errorText += '・携帯番号'+telFlag;
			alert(errorText);
			return false;
		} else {
			return true;
		};
	};

	/* サブ関数 */
	function initStep() {
		$('.meter_inner').width(1/layerLength*100+'%');
	}

	function discBtn(){
		trgElm = layer.eq(current).find('input, select, .slideTrg');
		layerValid();
		trgElm.on('change', function(event) {
			layerValid();
		});
	}

		function layerValid() {
			if($('.layer').eq(current).find('.inputArea dd').hasClass('error')){
				$('.subBtn').eq(current).prop('disabled', true);
			}else{
				emptyFlag = [];
				inputElm = trgElm.map(function(index, elem) {
					return ($(this).hasClass('slideTrg')) ? $(this).text() : $(this).val() ;
				}).get();
				for (var i = 0; i < inputElm.length; i++) {
					inputElm[i] = inputElm[i].replace(/\s/g, '');
					thisFlag = (inputElm[i] == '選択してください' || inputElm[i] == 'null' || inputElm[i] == '') ? true : false ;
					emptyFlag.push(thisFlag);
				}
				layerFlag[current] = ($.inArray(true, emptyFlag) >= 0) ? false : true ;
				$('.subBtn').eq(current).prop('disabled', !layerFlag[current]);
			}
		}

	function trnsNext() {
		if (layerLength != current+1) {
			slideInLayer();
			setTimeout(function(){
				cover.hide();
				discLayer();
			}, 200);
			discBtn();
		} else {
			return false;
		}
	}

		function slideInLayer() {
			cover.show();
			nextActive();
		}

			function nextActive() {
				layer.eq(current+1).show(0, function() {
					$(this).addClass('active');
				});
				discStep('next');
				current++;
				setGroup = 'first';
			}

				function discStep(e) {
					numerator = step.find('.txt span');
					if (e == 'next') {
						step.find('li').eq(current+2).addClass('active');
						numeratorNum = numerator.text()*1+1;
						targetStep = current+2;
					} else if (e == 'back') {
						step.find('li').eq(current+1).removeClass('active');
						numeratorNum = numerator.text()*1-1;
						targetStep = current;
					};
					meter_inner = targetStep/layerLength*100+'%';
					$('.meter_inner').width(meter_inner);
					numerator.text(numeratorNum);
				}

		function discLayer() {
			for (var i = 0; i < layerLength; i++) {
				if(i != current) layer.eq(i).hide();
			}
		}

	function trnsPrev() {
		cover.show();
		prevActive();
		setTimeout(function(){
			cover.hide();
			discLayer();
		}, 200);
		discBtn();
	}

		function prevActive() {
			layer.eq(current).removeClass('active').prev().show();
			discStep('back');
			current--;
			setGroup = 'first';
		}

	function panelIn() {
		pointY = $(window).scrollTop();
		$('body').css('overflow', 'hidden');
		panel.show(0, function() {
			$(this).addClass('active');
		});
		slctBtn.addClass('active');
		cover.show();
		setTimeout(function(){
			cover.hide();
		}, 400);
	}

	function panelOut() {
		$('body').attr('style', '');
		$(window).scrollTop(pointY);
		panel.removeClass('active');
		slctBtn.removeClass('active');
		cover.show();
		setTimeout(function(){
			cover.hide();
			panel.hide();
		}, 400);
	}

	function checkItem() {
		panel.find('li').each(function() {
			thisInput = $(this).find('input');
			if (thisInput.prop('checked')) {
				$(this).addClass('active');
				emptyFlag = false;
				value.push(thisInput.parent().text());
			} else {
				$(this).removeClass('active');
			}
		});
	}

	function discSelect() {
		if (emptyFlag == false) {
			slctBtn.find('button').prop('disabled', false);
		} else {
			slctBtn.find('button').prop('disabled', true);
		}
	}

	function inputValue() {
		outTxt = value[0];
		if (value.length > 1) outTxt += ' 他';
		$('[data-panelname="'+panelName+'"]').html(outTxt).trigger('change');
	}

	function discStepNum() {
		step.find('.num:hidden').show();
		initStep();
	}

	/* メールアドレスエラー表示 ※カーソルを外れた時用 */
	function mailchk(){
		val = inputMail.val();
		ret = is_mail(val);
		if((val!="")&&(ret)){
			$('#fMail').removeClass('js-check-error');
			return true;
		}else{
			$('#fMail').addClass('js-check-error');
			return false;
		}
	};

	/* メールアドレス：フォーマットチェック */
	function is_mail(val){
		if(val.match(/^([a-zA-Z0-9_\.\-\?])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/)){
			return true;
		}else{
			return false;
		}
	};

	/* 電話エラー表示 ※カーソルを外れた時用 */
	function tellchk(){
		val = inputTel.val();
		ret = is_tellno(val);
		if(ret==''){
			$('#fTel').removeClass('js-check-error');
		}else{
			$('#fTel').addClass('js-check-error');
		}
		return ret;
	};

	/* 電話番号：フォーマットチェック */
	function is_tellno(val){
		val = zen2han(val);
		val = val.replace(/ |\(|\)|-/g,"");
		errTxtTel = '';
		if(val.match(/^\d{11}$/)){
			if(val.match(/^0[5|7|8|9]0/)){
				errTxtTel = '';
			}else{
				errTxtTel = '\n　電話番号の形式が不正です。';
			}
		}else if(val.match(/^\d{10}$/)){
			if (val.match(/^0/)) {
				if (val.match(/^0[5|7|8|9]0/)) {
					errTxtTel = '\n　桁数が少ないです。';
				} else {
					errTxtTel = '';
				}
			} else {
				errTxtTel = '\n　電話番号の形式が不正です。';
			}
		}else{
			if(val.match(/\d{12,}$/)){
				errTxtTel = '\n　桁数が多いです。';
			}else　if(val.match(/\d{1,9}$/)){
				errTxtTel = '\n　桁数が少ないです。';
			}else{
				errTxtTel = '\n　電話番号は半角数字で入力してください。';
			}
		}
		return errTxtTel;
	};

	/* 全角を半角に */
	function zen2han(from) {
		var han = ['1','2','3','4','5','6','7','8','9','0','-',' ','(',')'];
		var zen = ['１','２','３','４','５','６','７','８','９','０','－','　','（','）'];
		var to = [];
		for( var i = 0 ; i < from.length ; i++ ) {
			for(var j = 0 ; j < zen.length ; j++ ) {
				if (from.charAt(i) == zen[j]) {
					to.push( han[j] );
					break;
				}
			}
			if (j == zen.length ) {
				to.push( from.charAt(i) );
			}
		}
		return to.join('');
	};

	var debug = false;
	if(debug){
		nextActive();
		nextActive();
	}

	// submit後に完了ボタンと戻るボタンの押下を不可にして再押下させない
	$('[name="entry_form"]').submit(function(){
		$('.btnField  :button').prop('disabled', true);
		return true;
	});

});
