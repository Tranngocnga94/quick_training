// åˆæœŸè¨­å®š
var formParts = 'input, select, textarea',
	speed = 200;

var birthFlg = false; // ç”Ÿå¹´æœˆæ—¥ãƒ•ãƒ©ã‚°

var form_init = function(){
	var form = $('#form'),
		stepStatus = form.find('.stepStatus li'),
		stepArea = form.find('.stepArea'),
		panelArea = form.find('.panelArea');

	// stepStatus
	stepStatus.eq(0).addClass('active');

	// stepArea
	stepArea.hide().eq(0).show();
	// debugæ™‚åˆæœŸã‚¹ãƒ†ãƒƒãƒ—æŒ‡å®šç”¨
	// stepArea.eq(0).removeClass('active').addClass('before');
	// stepArea.eq(1).show();
	// stepArea.eq(0).hide();
	// stepArea.eq(1).addClass('active');

	// tabindex
	stepArea.find('.input').find(formParts).attr('tabindex', -1);
	stepArea.eq(0).find('.input').find(formParts).attr('tabindex', 0);

	// panelArea
	panelArea.find('.agree').hide();
	panelArea.find('.prev').hide();
	panelArea.find('.send').hide();

	panelArea.find('.next').prop('disabled', true);
}

// stepã®ç§»å‹•
var step_move = function(btn){
	var form = $('#form'),
		stepStatus = form.find('.stepStatus li');
		stepArea = form.find('.stepArea'),
		panelArea = form.find('.panelArea'),
		step = $('.stepArea.active').attr('data-step'),
		activeStep = 0

	switch(step){
		case '1':
			stepStatus.eq(0).removeClass('active').addClass('finished');
			stepStatus.eq(1).addClass('active');
			stepArea.eq(0).removeClass('active').addClass('before');
			stepArea.eq(1).show();
			setTimeout(function(){
				stepArea.eq(0).hide();
				stepArea.eq(1).addClass('active');

				panelArea.find('.prev').show();
			}, speed);

			activeStep = 1;
			break;

		case '2':
			if(btn.hasClass('prev')){
				stepStatus.eq(0).removeClass('finished').addClass('active');
				stepStatus.eq(1).removeClass('active');
				stepArea.eq(0).show();
				stepArea.eq(1).removeClass('active');
				setTimeout(function(){
					stepArea.eq(0).addClass('active').removeClass('before');
					stepArea.eq(1).hide();
					panelArea.find('.prev').hide();
				}, speed);

				activeStep = 0;
			}else{
				stepStatus.eq(1).removeClass('active').addClass('finished');
				stepStatus.eq(2).addClass('active');
				stepArea.eq(1).removeClass('active').addClass('before');
				stepArea.eq(2).show();
				setTimeout(function(){
					stepArea.eq(1).hide();
					stepArea.eq(2).addClass('active');
					panelArea.find('.agree').show();
					panelArea.find('.next').hide();
					panelArea.find('.send').show();
				}, speed);

				activeStep = 2;
			}
			birthFlg = false;
			break;

		case '3':
			if(btn.hasClass('prev')){
				stepStatus.eq(1).removeClass('finished').addClass('active');
				stepStatus.eq(2).removeClass('active');
				stepArea.eq(1).show();
				stepArea.eq(2).removeClass('active');
				setTimeout(function(){
					stepArea.eq(1).addClass('active').removeClass('before');
					stepArea.eq(2).hide();
					panelArea.find('.agree').hide();
					panelArea.find('.next').show();
					panelArea.find('.send').hide();
				}, speed);

				activeStep = 1;
			}else{
				stepStatus.eq(2).removeClass('active').addClass('finished');
				stepStatus.eq(3).addClass('active');
				stepArea.eq(2).removeClass('active').addClass('before');
				stepArea.eq(3).show();
				setTimeout(function(){
					stepArea.eq(2).hide();
					stepArea.eq(3).addClass('active');
				}, speed);

				activeStep = 3;
			}
			break;

	}

	// tabindex
	stepArea.find('.input').find(formParts).attr('tabindex', -1);
	stepArea.eq(activeStep).find('.input').find(formParts).attr('tabindex', 0);
	// ãƒœã‚¿ãƒ³ã®åˆ¤å®š
	input_check(stepArea.eq(activeStep).find('input, select').eq(0), true);

}

// modalã®é¸æŠžå‡¦ç†
var modal_set = function(btn){
	var modal = btn.parents('.modalArea'),
		type = modal.attr('id');
	var form = $('#form'),
		stepArea = form.find('.stepArea');

	switch(type){
		// ãŠä½ã¾ã„
		case 'prefModal':
			var setVal = modal.find('input:checked').next().text(),
				target = $('.selectPref');

			if(!setVal){
				setVal = 'é¸æŠžã™ã‚‹'
			}
			target.val(setVal).change();

			break;

		// å¸Œæœ›å‹¤å‹™åœ°
		case 'hopeModal':
			var setVal = modal.find('input:checked').map(function(i){
				return $(this).next().text();
			}).get().join('ï½œ');

			var target = $('.hopePref').next('.viewTxt');
			if(setVal.length > 18){
				setVal = setVal.substr(0,(18)) + 'â€¦';
			}
			target.val(setVal).change();

			break;
	}

}

// modalã®é–‰ã˜ã‚‹å‡¦ç†
var modal_close = function(btn){
	var modal = btn.parents('.modalArea'),
		type = modal.attr('id');

	switch(type){
		// ãŠä½ã¾ã„
		case 'prefModal':
			break;

		// å¸Œæœ›å‹¤å‹™åœ°
		case 'hopeModal':
			var target = $('.hopePref').next('.viewTxt');
			target.change();

			break;
	}
}

// ã‚¨ãƒ©ãƒ¼ãƒã‚§ãƒƒã‚¯
var input_check = function(self, skip){
	var area = self.parents('.stepArea'),
		target = self.parents('.input'),
		check = target.attr('data-check'),
		input = area.find('.input'),
		inputFlg = false,
		sendBtn = $('.panelArea').find('.send');

	// ç¾åœ¨é¸æŠžã—ãŸé …ç›®ã®å‰ã¾ã§ã®ã‚¨ãƒ©ãƒ¼ãƒã‚§ãƒƒã‚¯ï¼ˆé¸æŠžä¸­ä»¥é™ã®é …ç›®ã¯åˆ¤å®šã—ãªã„ï¼‰
	for(var i = 0; i <= target.index(); i++) {
		// skipãŒtrueãªã‚‰ã‚¨ãƒ©ãƒ¼ãƒã‚§ãƒƒã‚¯ã¯è¡Œã‚ãªã„
		if(skip == true){
			break;
		}

		switch(check){
			// ã”çµŒé¨“ Step1
			case 'require_select':
				var val = input.eq(i).find('input, select').val(),
					box = input.eq(i).find('dd'),
					subTarget = self.parents('.license').find('.checkList li').length,
					subInput = area.find('.license').find('.checkList li');

				if(!val || val == 'é¸æŠžã™ã‚‹'){
					if(!box.hasClass('error')){
						box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">ã€æ³¨ã€‘æœªé¸æŠžã§ã™<p>')
					}
				}else{
					// å…¥åŠ›OKã§ã‚ã‚Œã°'pass'ã¨ã„ã†classã‚’ä»˜ã‘ã‚‹
					box.removeClass('error').addClass('pass').find('.errorTxt').remove();
					box.find('.selectModal').text("é¸æŠžæ¸ˆã¿");
				}

				for(var j = 0; j < subTarget-1; j++) {
					var subVal = subInput.eq(j).find('input'),
						subBox = subInput.parents('dd');
					// ã‚¨ãƒ©ãƒ¼ãƒ¡ãƒƒã‚»ãƒ¼ã‚¸å‰Šé™¤
					subBox.removeClass('error').find('p').remove();
					if(!subVal.prop('checked')){
						if(!subBox.hasClass('error')){
							subBox.removeClass('pass').addClass('error').prepend('<p class="errorTxt">ã€æ³¨ã€‘æœªé¸æŠžã§ã™<p>');
						}
					}else{
						// å…¥åŠ›OKã§ã‚ã‚Œã°'pass'ã¨ã„ã†classã‚’ä»˜ã‘ã‚‹
						subBox.removeClass('error').addClass('pass').find('p').remove();
						break;
					}
				}

				break;

			// ãƒ—ãƒ­ãƒ•ã‚£ãƒ¼ãƒ« Step2
			case 'require_profile':
				var set = input.eq(i).find('input, select'),
					box = input.eq(i).find('dd'),
					setFlg = false;
				for(var j = 0; j < set.length; j++) {
					var val = set.eq(j).val();
					setFlg = false;

					// ã‚¨ãƒ©ãƒ¼ãƒ¡ãƒƒã‚»ãƒ¼ã‚¸å‰Šé™¤
					box.removeClass('error').find('p').remove();

					// ä¸€ã¤ã§ã‚‚é¸æŠžã•ã‚Œã¦ãªã„é …ç›®ãŒã‚ã‚Œã°ã‚¨ãƒ©ãƒ¼
					if(!val){
						if(!box.hasClass('error')){
							if(set.prop('tagName') == 'SELECT'){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">ã€æ³¨ã€‘æœªé¸æŠžã§ã™<p>')
							}else{
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">ã€æ³¨ã€‘æœªå…¥åŠ›ã§ã™<p>')
							}
						}

						// ã‚¨ãƒ©ãƒ¼
						setFlg = true;
						break;
					}else{
						if(set.prop('name') == '_birth_y' || set.prop('name') == '_birth_m' || set.prop('name') == '_birth_d'){
							// ç”Ÿå¹´æœˆæ—¥åˆ¤å®š
							birth_y = input.eq(i).find('select[name="_birth_y"]').val(),
							birth_m = input.eq(i).find('select[name="_birth_m"]').val(),
							birth_d = input.eq(i).find('select[name="_birth_d"]').val(),
							birth = new Date(birth_y, birth_m-1, birth_d);

							// ç”Ÿå¹´æœˆæ—¥ã®å­˜åœ¨ãƒã‚§ãƒƒã‚¯
							if(!(birth.getFullYear()==birth_y && birth.getMonth()==birth_m-1 && birth.getDate()==birth_d)){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">ã€æ³¨ã€‘ç”Ÿå¹´æœˆæ—¥ãŒæ­£ã—ãã‚ã‚Šã¾ã›ã‚“ã€‚<p>')
								// ã‚¨ãƒ©ãƒ¼
								setFlg = true;
								break;
							}

						}else if(set.prop('name') == 'surname_ruby' || set.prop('name') == 'firstname_ruby'){
							// ã‚«ãƒŠåˆ¤å®š
							if(!val.match(/^[\u30a0-\u30ff\u3040-\u309f]+$/)){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">ã€æ³¨ã€‘ã²ã‚‰ãŒãªã¾ãŸã¯ã‚«ã‚¿ã‚«ãƒŠã§ã”å…¥åŠ›ãã ã•ã„ã€‚<p>')
								// ã‚¨ãƒ©ãƒ¼
								setFlg = true;
								break;
							}
						}
					}
				}
				if(!setFlg){
					// å…¥åŠ›OKã§ã‚ã‚Œã°'pass'ã¨ã„ã†classã‚’ä»˜ã‘ã‚‹
					box.addClass('pass');
				}

				break;

			// ãƒ—ãƒ­ãƒ•ã‚£ãƒ¼ãƒ« Step3
			case 'require_profile_contact':
				var set = input.eq(i).find('input, select'),
					box = input.eq(i).find('dd'),
					setFlg = false;

				for(var j = 0; j < set.length; j++) {
					var val = set.eq(j).val();
					setFlg = false;

					// ã‚¨ãƒ©ãƒ¼ãƒ¡ãƒƒã‚»ãƒ¼ã‚¸å‰Šé™¤
					box.removeClass('error').find('p').remove();

					// ä¸€ã¤ã§ã‚‚é¸æŠžã•ã‚Œã¦ãªã„é …ç›®ãŒã‚ã‚Œã°ã‚¨ãƒ©ãƒ¼
					if(!val){
						if(!box.hasClass('error')){
							if(set.prop('tagName') == 'SELECT'){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">ã€æ³¨ã€‘æœªé¸æŠžã§ã™<p>')
							}else{
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">ã€æ³¨ã€‘æœªå…¥åŠ›ã§ã™<p>')
							}
						}

						// ã‚¨ãƒ©ãƒ¼
						setFlg = true;
						break;
					}else{
						if(set.prop('name') == 'email'){
							// emailåˆ¤å®š
							if(!val.match(/.+@.+\..+/)){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">ã€æ³¨ã€‘ãƒ¡ãƒ¼ãƒ«ã‚¢ãƒ‰ãƒ¬ã‚¹ãŒæ­£ã—ãã‚ã‚Šã¾ã›ã‚“<p>')
								// ã‚¨ãƒ©ãƒ¼
								setFlg = true;
								break;
							}
						}else if(set.prop('name') == 'tel2'){
							// telåˆ¤å®š
							// ãƒã‚¤ãƒ•ãƒ³ãŒå…¥ã£ã¦ã„ãŸã‚‰å¼·åˆ¶çš„ã«å‰Šé™¤
							val = val.replace(/[â”.*â€.*â€•.*ï¼.*\?.*ãƒ¼.*\-]/gi,'');
							// å…¨è§’ â‡’ åŠè§’
							val = val.replace(/[ï¼¡-ï¼ºï½-ï½šï¼-ï¼™]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
							set.eq(j).val(val);

							var errTxt = '';
							if(val.match(/^\d{11}$/)){
								if(val.match(/^0[5|7|8|9]0/)){
									errTxt = '';
								}else{
									errTxt = 'ã€æ³¨ã€‘é›»è©±ç•ªå·ã®å½¢å¼ãŒä¸æ­£ã§ã™';
								}
							}else if(val.match(/^\d{10}$/)){
								if (val.match(/^0/)) {
									if (val.match(/^0[5|7|8|9]0/)) {
										errTxt = 'ã€æ³¨ã€‘æ¡æ•°ãŒå°‘ãªã„ã§ã™';
									} else {
										errTxt = '';
									}
								} else {
									errTxt = 'ã€æ³¨ã€‘é›»è©±ç•ªå·ã®å½¢å¼ãŒä¸æ­£ã§ã™';
								}
							}else{
								if(val.match(/\d{12,}$/)){
									errTxt = 'ã€æ³¨ã€‘æ¡æ•°ãŒå¤šã„ã§ã™';
								}else if(val.match(/\d{1,9}$/)){
									errTxt = 'ã€æ³¨ã€‘æ¡æ•°ãŒå°‘ãªã„ã§ã™';
								}else{
									errTxt = 'ã€æ³¨ã€‘é›»è©±ç•ªå·ã¯åŠè§’æ•°å­—ã§å…¥åŠ›ã—ã¦ãã ã•ã„';
								}
							}

							if(errTxt){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">'+errTxt+'<p>')
								// ã‚¨ãƒ©ãƒ¼
								setFlg = true;
								break;
							}
						}
					}
				}
				if(!setFlg){
					// å…¥åŠ›OKã§ã‚ã‚Œã°'pass'ã¨ã„ã†classã‚’ä»˜ã‘ã‚‹
					box.addClass('pass');
				}

				break;
		}

		// skipã«exãŒå…¥ã£ã¦ã„ã‚Œã°ã€é¸æŠžä¸­ã®é …ç›®ã¯åˆ¤å®šã—ãªã„
		if(skip == 'ex' && i == target.index() - 1){
			break;
		}
	}

	// passã®classåˆ¤å®šï¼ˆã“ã‚Œã¯é …ç›®å…¨ã¦ï¼‰
	for(var i = 0; i < input.length; i++) {
		if(check == 'require_experience'){
			var box = input.eq(i).find('.inputWrapper');
		}else{
			var box = input.eq(i).find('dd');
		}
		if(!box.hasClass('pass')){
			// passãŒä»˜ã„ã¦ã„ãªã„é …ç›®ãŒã‚ã‚Œã°ã‚¨ãƒ©ãƒ¼ã¨ã™ã‚‹
			inputFlg = true;
			break;
		}
	}

	// ã‚¨ãƒ©ãƒ¼ãŒãªã‘ã‚Œã°ãƒœã‚¿ãƒ³æ´»æ€§åŒ–
	if(sendBtn.is(':visible') && sendBtn.is(':disabled')) {
		setTimeout(function(){
			$('.panelArea').find('.next, .send').prop('disabled', inputFlg);
		}, speed);
	}else{
		$('.panelArea').find('.next, .send').prop('disabled', inputFlg);
	}
}

$(function(){
	// form init
	form_init();

	// btnã®å‡¦ç†
	$('.btnArea').find('button').on('click', function(){
		if($(this).hasClass('send')){
			// form_submit();
		}else{
			step_move($(this));
			errorActive = undefined;
		}
	});

	// ãƒ¢ãƒ¼ãƒ€ãƒ«è¡¨ç¤º
	$('.selectModal').on('click', function(){
		var self = $(this);

		if(self.hasClass('selectPref')){
			$('#prefModal').addClass('active').css({'z-index':100});
		}else if(self.hasClass('hopePref')){
			$('#hopeModal').addClass('active').css({'z-index':100});
		}
	});

// modal control Start //
	var modal = $('.modalArea'),
		modalInner = modal.find('.inner'),
		modalInput = modal.find('input'),
		modalBtn = modal.find('.btn'),
		modalClose = modal.find('.closeIcon');

	// ãƒ¢ãƒ¼ãƒ€ãƒ« æ±ºå®š
	modalBtn.on('click', function(){
		modal.removeClass('active').animate({'z-index': -1},500);

		// ãƒ¢ãƒ¼ãƒ€ãƒ«ã®é¸æŠžè‚¢é¸æŠžå‡¦ç†
		modal_set($(this));

		return false;
	});

	// ãƒ¢ãƒ¼ãƒ€ãƒ« é–‰ã˜ã‚‹
	modalClose.on('click', function(){
		modal.removeClass('active').animate({'z-index': -1},500);

		// ãƒ¢ãƒ¼ãƒ€ãƒ«ã®é–‰ã˜ã‚‹å‡¦ç†
		modal_close($(this));
	});
	modal.on('click', function(){
		modal.removeClass('active').animate({'z-index': -1},500);

		// ãƒ¢ãƒ¼ãƒ€ãƒ«ã®é–‰ã˜ã‚‹å‡¦ç†
		modal_close($(this).find('.closeIcon'));
	});
	modalInner.on('click', function(event){
		event.stopPropagation();
	});

	// ãƒ¢ãƒ¼ãƒ€ãƒ«å†…ã®ãƒãƒªãƒ‡ãƒ¼ã‚·ãƒ§ãƒ³ãƒã‚§ãƒƒã‚¯
	modalInput.on('change blur', function(){
		var self = $(this).parents('.modalArea');
		var setVal = self.find('input:checked').map(function(i){
			return $(this).next().text();
		}).get().join();

		if(setVal){
			self.find('.btn').prop('disabled', false);
		}else{
			self.find('.btn').prop('disabled', true);
		}
	});

// modal control End //

// ãŠæŒã¡ã®è³‡æ ¼ Start //
	var lic = $('.license .checkList li input'),
		lic_other = $('.license_other');

	lic.on('change', function(){
		// ãã®ä»–ãŒé¸æŠžã•ã‚ŒãŸå ´åˆ
		if($(this).val() == 6){
			if($(this).prop('checked')) {
				lic_other.show();
			}else{
				lic_other.hide();
			}
		}
	});
// ãŠæŒã¡ã®è³‡æ ¼ End //

// ã“ã‚Œã¾ã§ã®ã”çµŒé¨“ Start //
	var exp = $('.experience'),
		direct = exp.find('#directForm'),
		fileup = exp.find('#fireupForm'),
		fileInput = $('#career_file'),
		textarea = $('#directTxt'),
		txtCount = direct.find('.textCount .num');

	// å…¥åŠ›é …ç›®è¡¨ç¤º
	exp.find('input').on('change', function(){
		if($('input[name="history"]:checked').val() == 1){
			direct.hide();
			fileup.show();
		}else{
			fileup.hide();
			direct.show();
		}
	});

	// è·å‹™çµŒæ­´æ›¸ã‚’æ·»ä»˜ã™ã‚‹
	fileInput.change(function() {
		fileup.find('.txt').html($(this).val().replace("C:\\fakepath\\", ""));
	});

	fileup.find('.btn').on('click', function(){
		fileInput.click();
		if(fileup.hasClass('first')){
			setTimeout(function(){
				fileup.removeClass('first');
				fileup.prepend('<p class="errorTxt">ã€æ³¨ã€‘æœªé¸æŠžã§ã™<p>');
			}, 200);
		}
	});
// ã“ã‚Œã¾ã§ã®ã”çµŒæ­´ End //

// ã‚¨ãƒ©ãƒ¼ãƒã‚§ãƒƒã‚¯ Start //
	var stepArea = $('.stepArea'),
		stepInput = stepArea.find('.input');
	var errorActive; // ã‚¨ãƒ©ãƒ¼åˆ¤å®šä¸­ã®é …ç›®

	stepArea.find('.input').find(formParts).on('focus change blur', function(e){
		var target = $(this).parents('.input'),
			self = $(this);

		// selectã®å ´åˆã¯è¦ªè¦ç´ ã®ãƒ©ãƒ™ãƒ«ã§åˆ¤å®š
		if(e.target.tagName == 'SELECT'){
			self = self.parents('.selectLabel');
		}

		// focusæ™‚ã®å‡¦ç†
		if(e.type == 'focus'){
			// ç”Ÿå¹´æœˆæ—¥ã®ã©ã‚Œã‹ã‚’focusã—ãŸã¨ãã«ãƒ•ãƒ©ã‚°ã‚’ç«‹ã¦ã‚‹
			if($(e.target).attr('name') == '_birth_y' || 
			$(e.target).attr('name') == '_birth_m' || 
			$(e.target).attr('name') == '_birth_d'){
				birthFlg = true;
			}else{
				birthFlg = false;
			}

			// ä¸€ç•ªæœ€åˆ
			if(errorActive == undefined){
				errorActive = target.index();
			}else if(errorActive != target.index()){
				input_check($(this), 'ex');
				errorActive = target.index();
			}
		}else{
			// åˆ¤å®šã™ã‚‹é …ç›®ãŒ2ä»¶ä»¥ä¸Šã‚ã£ãŸã‚‰
			if(target.find('input, select').length > 1 && !target.hasClass('experience')){
				if(errorActive != target.index()){
					input_check($(this));
					errorActive = target.index();
				}else if(self.index() == target.find('input, select').length - 1){
					input_check($(this));
				}else if(self.index() == 3 || self.index() == 2){
					input_check($(this));
				}else if(self.attr('type') == 'checkbox'){
					input_check($(this));
				}
			}else{
				input_check($(this));
			}
		}
	});

	var form = $('#form');

	form.on('click', function(e){
		if(e.target.tagName != 'SELECT' &&
		 	e.target.tagName != 'INPUT' &&
		  	e.target.tagName != 'OPTION' &&
		  	e.target.tagName != 'BUTTON' &&
		   	e.target.tagName != 'SPAN'){
			if(birthFlg){
				input_check($('select[name="_birth_d"]'));
			}
		}
	});

// ã‚¨ãƒ©ãƒ¼ãƒã‚§ãƒƒã‚¯ End //

	// submitå¾Œã«å®Œäº†ãƒœã‚¿ãƒ³ã¨æˆ»ã‚‹ãƒœã‚¿ãƒ³ã®æŠ¼ä¸‹ã‚’ä¸å¯ã«ã—ã¦å†æŠ¼ä¸‹ã•ã›ãªã„
	$('#form').submit(function(){
		$('.btnArea  :button').prop('disabled', true);
		return true;
	});


// debugç”¨
	var debug = false;

	if(debug){
		$('.panelArea').append('<button style="position:fixed; left:135px; top:210px;" class="dummy1">step1</button>');
		$('.panelArea').append('<button style="position:fixed; left:135px; top:240px;" class="dummy2">step2</button>');
		$('.panelArea').append('<button style="position:fixed; left:135px; top:270px;" class="dummy3">step3</button>');

		$('.dummy1').on('click', function(){
			// ãŠä½ã¾ã„
			$('input[name="pr_id"]').eq(1).click();
			$('#prefModal').find('.btn').click();

			// å¸Œæœ›å‹¤å‹™åœ°
			$('input[name="_hope_pref_id[]"]').eq(1).click();
			$('#hopeModal').find('.btn').click();

			return false;
		});


		$('.dummy2').on('click', function(){
			// ç”Ÿå¹´æœˆæ—¥
			$('select[name="_birth_y"]').val('1990').change();
			$('select[name="_birth_m"]').val('10').change();
			$('select[name="_birth_d"]').val('10').change();

			// ãŠåå‰
			$('input[name="surname"]').val('ãƒ†ã‚¹ãƒˆãƒ†ã‚¹ãƒˆ').change();
			$('input[name="firstname"]').val('ãƒ†ã‚¹ãƒˆ').change();

			// ãŠãªã¾ãˆ
			$('input[name="surname_ruby"]').val('ã¦ã™ã¨ã¦ã™ã¨').change();
			$('input[name="firstname_ruby"]').val('ã¦ã™ã¨').change();

			return false;
		});


		$('.dummy3').on('click', function(){
			// ãƒ¡ãƒ¼ãƒ«ã‚¢ãƒ‰ãƒ¬ã‚¹
			$('input[name="email"]').val('form.debug@gmail.com').change();

			// é›»è©±ç•ªå·
			$('input[name="tel2"]').val('08000000000').change();

			return false;
		});
	}

});